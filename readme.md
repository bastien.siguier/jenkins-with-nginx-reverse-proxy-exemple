# installer Jenkins
## start
```
    sudo wget -O /usr/share/keyrings/jenkins-keyring.asc \
    https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key
    echo "deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc]" \
    https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
    /etc/apt/sources.list.d/jenkins.list > /dev/null
    sudo apt-get update
    sudo apt-get install jenkins
```

## install java
```
    sudo apt update
    sudo apt install fontconfig openjdk-17-jre
    java -version

    sudo systemctl enable jenkins
    sudo systemctl start jenkins
```
# install nginx
    sudo apt install nginx

# generate ssl certificat
rediriger son nom de domaine sur le serveur
```
    sudo apt-get install letsencrypt
    letsencrypt certonly --standalone --agree-tos --email mon-mail@test.com -d exemple.com -d www.exemple.com
```

# Créer le fichier de config
    touch /etc/nginx/sites-available/01-www.exemple.com.conf

    qui doit contenir : 
```
    upstream backend_jenkins{
        server 127.0.0.1:8080;
    }

    server {
        listen    80;
        server_name    www.exemple.com exemple.com;

        #Redirige toutes les requêtes http vers https
        return 301 https://$host$request_uri;
    }

    server {
        listen 443 ssl;
        ssl_certificate /etc/letsencrypt/live/exemple.com/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/exemple.com/privkey.pem;

        location / {
            include proxy_params;
            proxy_pass http://backend_jenkins;
        }
    }
```

# Activer server block et restart nginx
```
    sudo ln -s /etc/nginx/sites-available/01-www.exemple.com.conf /etc/nginx/sites-enabled/01-www.exemple.com.conf
    sudo systemctl reload nginx
 ```